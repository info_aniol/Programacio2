package edu.ub.prog2.GarciaAniolTarresXavier.model;
import java.io.File;
import java.util.Date;

/**
 * La classe FitxerMultimedia hereda de File i l'amplia amb un atribut 
 * descripció i diversos mètodes propis, per tal gestionar els fitxers i obtenir-ne
 * informació.
 */
public class FitxerMultimedia extends File
{

    private String descripcio;
    
    /**
     * Constructor de fitxer multimedia
     * @param cami Path del arxiu
     */
    public FitxerMultimedia(String cami)
    {
          super(cami);
          this.descripcio = "";
    }

    /**
     * Getter per obtenir el path de l'arxiu
     * @return String amb el path absolut de l'arxiu
     */
    public String getCami()
    {
        return this.getAbsolutePath();
    }
    
    /**
     * Getter per obtenir el nom del fitxer i la seva extensió
     * @return String amb el nom del fitxer i la extensió
     */
    public String getNom()
    {
        //D'aquesta manera mirem si el fitxer té extensió o si el nom conté punts
        int pos = this.getName().lastIndexOf(".");
 
        if (pos > 0)//Si te un punt al 0, aquest pertany al nom del fitxer
        {
            return this.getName().substring(0,pos);
        }
        
        else 
        {
            return this.getName();
        }
        
        //Altre mode, pero pitjor ja que no contempla el cas on hi ha punts al nom del fitxer
        //return this.getName().split("\\.")[0];
    }
    
    /**
     * Getter per obtenir la extensió del fitxer
     * @return String amb l'extensió del fitxer
     */
    public String getExtensio()
    {
        //Contemplem els casos on no té extensió i el nom del fitxer conté punts
        int pos = this.getName().lastIndexOf(".");
        if (pos > 0)
        {
            return this.getName().substring(pos+1);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Setter de la data d'ultima modificació per la data actual
     */
    public void setDateActual()
    {
        Date date = new Date();
        this.setLastModified(date.getTime());
    }
    
    /**
     * Getter de l'última data de modificació del fitxer
     * @return Objecte de tipus date amb la data d'última modificació
     */
    public Date getDate()
    {
        return new Date(this.lastModified());
    }
    
    /**
     * Setter de la descripció del fitxer
     * @param descripcio String amb la descripció del fitxer
     */
    public void setDescripcio(String descripcio)
    {
        this.descripcio = descripcio;
    }
    
    /**
     * Getter de la descripció del fitxer
     * @return String amb la descripció del fitxer
     */
    public String getDescripcio()
    {
        return descripcio;
    }
    
    /**
     * Mètode per comprovar si dos FitxersMultimedia són iguals
     * @param fitxer El fitxer amb el qual volem comparar el fitxer actual
     * @return <code>true</code> si els fitxers són iguals i <code>false</code>
     * si no ho són
     */
    public boolean equals(Object fitxer)
    {
        if (fitxer instanceof FitxerMultimedia)
        {
            FitxerMultimedia f = (FitxerMultimedia) fitxer;
            return (this.getCami().equals(f.getCami()) && this.getDescripcio().equals((f.getDescripcio()))); 
        }
        return false;
        //Mirem si es refereixen al mateix arxiu amb el seu path
    }
    
    /**
     * Sobreescriptura del mètode toString de file
     * @return String amb totes les dades del fitxer
     */
    @Override
    public String toString()
    {
        return ("Camí: " + this.getCami() + "\n Nom: " + this.getNom() + "\n Extensió: " + this.getExtensio()
                + "\n Data última modificació: " + this.getDate() + "\n Descripció: " + this.getDescripcio() + "\n");
    }
}
