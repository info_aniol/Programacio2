package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;
import java.io.File;

public class Audio extends FitxerReproduible
{
    private File fitxerImatge;
    private int kbps;
    /**
     * Constructor de Audio
     * @param cami Path de l'àudio
     * @param fitxerImatge Ftxer de la caràtula de l'àudio
     * @param codec  El còdec de l'àudio
     * @param durada Dudrada de l'àudio
     * @param kbps  Bitrate (qualitat) de l'àudio en kilobits per second (kbps)
     * @param r Reproductor
     */
    public Audio(String cami, File fitxerImatge, String codec, float durada, int kbps, Reproductor r)      
    {
        super(cami, codec, durada, r);
        this.fitxerImatge = fitxerImatge;
        this.kbps = kbps;
    }
    
    /**
     * Getter del path de la caràtula
     * @return String amb el path de fitxerImatge
     */
    public String getImgPath()
    {
        return fitxerImatge.getPath();
    }
    
    
    /**
     * Getter de la bitrate de l'arxiu
     * @return Enter amb la bitrate de l'àudio
     */
    public int getKbps()
    {
        return this.kbps;
    }
    
    /**
     * Sobreescriptura del mètode toString
     * @return String amb tota la informació de l'àudio
     */
    @Override
    public String toString()
    {
       return (super.toString() + " Imatge caràtula: " + this.getImgPath() +  "\n Bitrate: " + this.getKbps() + "\n"); 
    }
    
    /**
     * Sobreescriptura de reproduir de FitxerReproduible
     */
    @Override
    public void reproduir()
    {
        
    }
}
