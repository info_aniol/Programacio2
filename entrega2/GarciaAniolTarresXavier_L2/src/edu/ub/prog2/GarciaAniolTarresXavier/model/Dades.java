package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;
import java.io.File;
import java.io.Serializable;
import edu.ub.prog2.utils.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Dades implements Serializable
{
    private CarpetaFitxers biblioteca;
    
    /**
     * Constructor per defecte de Dades
     */
    public Dades()
    {
        biblioteca = new BibliotecaFitxersMultimedia();
    }
    
    /**
     * Metode per afegir un video a la biblioteca.
     * @param cami
     * @param descripcio
     * @param codec
     * @param durada
     * @param alcada
     * @param amplada
     * @param fps
     * @param r
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    public void addVideo(String cami, String descripcio, String codec, float durada, int alcada, int amplada, float fps, Reproductor r) throws AplicacioException 
    {
        Video video = new Video(cami, codec, durada, alcada, amplada, fps, r);
        video.setDescripcio(descripcio);
        try
        {
            biblioteca.addFitxer(video);
        }
        catch(AplicacioException e) //Si es llença una excpeció la tornem a tirar amunt.
        {
            throw e;
        }
    }
    
    /**
     * Mètode per afegir un àudio a la biblioteca
     * @param cami
     * @param descripcio
     * @param fitxerImatge
     * @param codec
     * @param durada
     * @param kbps
     * @param r
     * @throws AplicacioException 
     */
    public void addAudio(String cami, String descripcio, File fitxerImatge, String codec, float durada, int kbps, Reproductor r) throws AplicacioException
    {
        Audio audio = new Audio(cami, fitxerImatge, codec, durada, kbps, r);
        audio.setDescripcio(descripcio);
        try
        {
            biblioteca.addFitxer(audio);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Mètode per eliminar un fitxer donada la seva id
     * @param id Posició que ocupa el fitxer a biblioteca
     * @throws AplicacioException 
     */
    public void removeFitxer(int id) throws AplicacioException
    {
        try
        {
            biblioteca.removeFitxer(id);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
            
    }
    
    /**
     * Metode per guardar la dades
     * @param f
     * @throws AplicacioException 
     */
    public void save(File f) throws AplicacioException
    {
        
        FileOutputStream fileStream = null;
        ObjectOutputStream os = null;
        try
        {
            //Intentem obrir el fitxer i carregar-hi l'objecte Dades actual
            fileStream = new FileOutputStream(f);
            os = new ObjectOutputStream(fileStream);
            os.writeObject(this);
        }
        catch(FileNotFoundException e) //Si no es troba el fitxer
        {
            throw new AplicacioException("Fitxer no trobat");            
        }
        catch(IOException e) //Si no es pot obrir o escriure-hi
        {
            throw new AplicacioException("Problema d'escriptura");
        }
        finally //Hagi tirar excepció o no
        {
            try
            {
                if(fileStream != null) //Si el fitxer no és null (si s'ha obert)
                {
                    fileStream.close(); //Tanquem el fitxer
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar el fitxer");
            }
            
            try
            {
                if(fileStream != null)
                {
                    os.close(); //Tanquem l'ObjectStream
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar l'Output Stream");
            }
        } 
    }
    
    /**
     * Metode per recuperar les dades d'un fitxer
     * @param f fitxer d'on recuperar les dades
     * @return Objecte Dades amb les nostres dades
     * @throws AplicacioException
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static Dades recover(File f) throws AplicacioException, IOException, ClassNotFoundException
    {
        Dades d;
        InputStream fileStream = null;
        ObjectInputStream oi = null ;
        try
        {
            //Intentem obrir el fitxer
            fileStream = new FileInputStream(f);
            oi = new ObjectInputStream(fileStream);
            d = (Dades)oi.readObject(); //Llegim l'objecte i fem cast implícit a dades
            return d; //Retornem un objecte de tipus Dades
        }
        catch(FileNotFoundException e)
        {
            throw new AplicacioException("Fitxer no trobat");            
        }
        catch(IOException e)
        {
            throw new AplicacioException("No s'ha pogut recuperar");
        }
        finally
        {
            try
            {
                if(fileStream != null)
                {
                    fileStream.close(); //Intentem tancar el fitxer
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar el fitxer");
            }
            try
            {
                if(fileStream != null)
                {
                    oi.close(); //Intentem tancar l'ObjectStream
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar l'Input Stream");
            }
        }
    }
    
    /**
     * Mètode per recopilar tota la informació de cadascun dels fitxers
     * @return List de Strings amb els toString de cada ftxer
     */
    public List<String> mostrarBiblioteca()
    {
        List<String> biblio= new ArrayList<String>();
        for(int i = 0; i < biblioteca.getSize(); i++) //Recorrem biblioteca
        {
            biblio.add(biblioteca.getAt(i).toString()); //Cridem toString de cada fitxer de biblioteca.
        }
        return biblio;
    }
    
    /**
     * Sobreescriptra del mètode toString
     * @return Una única String amb tota la informació dels fitxers.
     */
    @Override
    public String toString()
    {
        return biblioteca.toString();
    }
}

