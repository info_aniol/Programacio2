package edu.ub.prog2.GarciaAniolTarresXavier.vista;
import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Controlador;
import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;
import edu.ub.prog2.utils.AplicacioException;
import edu.ub.prog2.utils.Menu;
import java.io.File;
import java.util.List;
import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * Classe principal del reproductor multimèdia. S'encarrega de gestionar el 
 * menu, l'entrada de l'usuari i la lógica del programa.
 */
public class AplicacioUB2 
{
    //Menu Principal
    private static enum OpcionsMenuPrincipal{GESTIO,GUARDAR,RECUPERAR,SORTIR};
    private static String[] desMenuPrincipal =  {"Gestió Biblioteca", "Guardar Dades",
                                                "Recuperar Dades", "Sortir"};
    
    //Menu Gestio de la Biblioteca
    private static enum OpcionsMenuGestio{AFEGIR,MOSTRAR,ELIMINAR,ANTERIOR};
    private static String[] desMenuGestio = {"Afegir fitxer multimèdia a la biblioteca", "Mostrar Biblioteca",
                                            "Eliminar fitxer multimèdia", "Menú anterior"};

    private static enum OpcionsMenuAfegir{VIDEO,AUDIO,ANTERIOR};
    private static String[] desMenuAfegir = {"Afegir vídeo", "Afegir àudio", "Menú anterior"};
    
    private Menu <OpcionsMenuPrincipal> menuPrincipal;
    private Menu <OpcionsMenuGestio> menuGestio;
    private Menu <OpcionsMenuAfegir> menuAfegir;
    
    private OpcionsMenuPrincipal opcioPrincipal;
    private OpcionsMenuGestio opcioGestio;
    private OpcionsMenuAfegir opcioAfegir;
    
    private Controlador controlador;

   
    /**
     * Constructor per defecte, buit ja que no hi ha atributs a inicialitzar.
     */
    public AplicacioUB2()
    {

    }  
    
    /**
     * Mètode principal de gestió del programa. Crea els menus i gestiona l'entrada
     * de l'usuari.
     * @param sc
     */
    public void gestioAplicacioUB(Scanner sc)
    {
        //Creació Menus
        menuPrincipal = new Menu("Menú Principal", OpcionsMenuPrincipal.values());
        menuGestio = new Menu ("Menú Gestio Biblioteca", OpcionsMenuGestio.values());
        menuAfegir = new Menu ("Menú Afegir Fitxer", OpcionsMenuAfegir.values());
        
        menuPrincipal.setDescripcions(desMenuPrincipal);
        menuGestio.setDescripcions(desMenuGestio);
        menuAfegir.setDescripcions(desMenuAfegir);
        
        //Creacio controlador
        controlador = new Controlador();
        do
        {
            menuPrincipal.mostrarMenu();
            opcioPrincipal = menuPrincipal.getOpcio(sc);
            menuPrincipal(opcioPrincipal, sc);
        }while(opcioPrincipal != OpcionsMenuPrincipal.SORTIR);
    }
    
    
    private void menuPrincipal(OpcionsMenuPrincipal opcioPrincipal, Scanner sc)
    {
        switch(opcioPrincipal)
        {
            case GESTIO:
            {
                do
                {
                    menuGestio.mostrarMenu();
                    opcioGestio = menuGestio.getOpcio(sc);
                    menuGestio(opcioGestio, sc);                               
                }while(opcioGestio != OpcionsMenuGestio.ANTERIOR);
                break;
            }

            case GUARDAR:
            {
                String path = demanarDades(sc, "Introdueix el nom del fitxer on guardar les dades");
                try
                {
                    controlador.save(path);
                    System.out.println("Dades guardades");
                }
                catch(AplicacioException e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            }

            case RECUPERAR:
            {
                try
                {
                    String path = demanarDades(sc, "Introdueix el nom del fitxer on hi ha les dades a recuperar:");
                    controlador.recover(path);
                    System.out.println("Dades recuperades");
                }
                catch(AplicacioException e)
                {
                    System.out.print(e.getMessage());
                }
                break;
            }

            case SORTIR:
            {
                System.out.println("Adeu");
                break;                    
            }                
        }
    }
    
    private void menuGestio(OpcionsMenuGestio opcioGestio, Scanner sc)
    {
        switch(opcioGestio)
        {
            case AFEGIR:
            {
                do
                {
                    menuAfegir.mostrarMenu();
                    opcioAfegir = menuAfegir.getOpcio(sc);
                    menuAfegir(opcioAfegir, sc);
                    
                }while(opcioAfegir != OpcionsMenuAfegir.ANTERIOR);
                break;
            }
            case MOSTRAR:
            {
                System.out.println("Biblioteca Fitxers:\n==============");
                List<String> biblioteca = controlador.mostrarBiblioteca();
                for (int i = 1; i <= biblioteca.size(); i++)
                {
                    System.out.println("[" + i + "] " + biblioteca.get(i-1));
                }
                break;
            }  
            case ELIMINAR:
            {
                System.out.println(controlador.toString());
                System.out.println("Introduiexi l'índex del fitxer que vol eliminar: ");
                int id = sc.nextInt();
                try
                {
                    controlador.removeFitxer(id);
                }
                catch(AplicacioException e)
                {
                    System.out.println(e.getMessage());
                }
                break;
            }  
            case ANTERIOR:
            {
                System.out.println("Tornant al menú principal");
                break;                            
            }
        }       
    }
    
    private void menuAfegir(OpcionsMenuAfegir opcioAfegir, Scanner sc)
    {
        switch(opcioAfegir)
        {
            case VIDEO:
            {                                            
                String cami = demanarDades(sc, "Introdueix el camí del video: ");
                String descripcio = demanarDades(sc, "Introdueix la descripcio del video: ");
                String codec = demanarDades(sc, "Introduiexi el codec del video: ");
                try
                {
                    System.out.println("Introduiexi la durada del video: ");
                    float durada = sc.nextFloat();
                    System.out.println("Introdueixi la resolució: [amplada alçada] ");
                    int amplada = sc.nextInt();
                    int alcada = sc.nextInt();
                    System.out.println("Introdueixi la framerate: ");
                    float fps = sc.nextFloat();
                    Reproductor r = new Reproductor();

                    try
                    {
                        controlador.addVideo(cami, descripcio, codec, durada, alcada, amplada, fps, r);
                    }
                    catch(AplicacioException e)
                    {
                        System.out.println(e.getMessage());
                    }
                }
                
                catch(InputMismatchException e)
                {
                    System.out.println("Miss match: El tipus d'entrada és diferent a l'esperat.");
                    sc.nextLine();
                }
                break;
            }
            case AUDIO:
            {
                String cami = demanarDades(sc, "Introdueix el camí de l'àudio: ");
                String descripcio = demanarDades(sc, "Introdueix la descripcio de l'àudio: ");
                String camiImatge = demanarDades(sc, "Introdueixi el camí de la caràtula: ");

                File fitxerImatge = new File(camiImatge);
                if(!fitxerImatge.exists())
                {
                    System.out.println("No s'ha trobat la caràtula.");
                }

                String codec = demanarDades(sc, "Introduiexi el codec de l'àudio: ");

                try
                {
                    System.out.println("Introduiexi la durada de l'àudio: ");
                    float durada = sc.nextFloat();

                    System.out.println("Introdueixi el bitrate (kbps):");
                    int kbps = sc.nextInt();

                    Reproductor r = new Reproductor();

                    try
                    {
                        controlador.addAudio(cami, descripcio, fitxerImatge, codec, durada, kbps, r);
                    }
                    catch(AplicacioException e)
                    {
                        System.out.println(e.getMessage());
                        
                    }
                }
                catch(InputMismatchException e)
                {
                    System.out.println("Miss match: El tipus d'entrada és diferent a l'esperat.");
                    sc.nextLine();
                }
                break;
            }  
            case ANTERIOR:
            {
                System.out.println("Tornant al menú gestió");
                break;                            
            }                          
        }       
    }
    
    /**
     * Funció per recollir l'entrada de l'usuari
     * @param sc objecte de tipus scanner per l'input de dades
     * @param missatge missatge a mostrar abans de demanar entrada
     * @return l'entrada de l'usuari
     */
    public static String demanarDades(Scanner sc, String missatge)
    {
        String temp;
        System.out.println(missatge);
        temp = sc.nextLine();
        return temp;
    }
}
