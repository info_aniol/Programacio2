package edu.ub.prog2.GarciaAniolTarresXavier.vista;
import java.util.Scanner;

/**
 * Classe inicial del projecte. Crea una instància de AplicacioUB1 i una de
 * Scanner amb les quals es gestionarà tota l'aplicació.
 */
public class IniciadorAplicacioUB 
{
    public static void main(String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        AplicacioUB1 aplicacio = new AplicacioUB1 ();
        aplicacio.gestioAplicacioUB(sc);
    }
}
