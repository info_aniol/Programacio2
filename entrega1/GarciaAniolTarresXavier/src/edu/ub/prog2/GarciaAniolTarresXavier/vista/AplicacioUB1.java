package edu.ub.prog2.GarciaAniolTarresXavier.vista;
import edu.ub.prog2.GarciaAniolTarresXavier.model.CarpetaFitxers;
import edu.ub.prog2.GarciaAniolTarresXavier.model.FitxerMultimedia;
import edu.ub.prog2.utils.Menu;
import java.util.Scanner;

/**
 * Classe principal del reproductor multimèdia. S'encarrega de gestionar la 
 * lògica del programa i l'entrada de l'usuari mitjançant un objecte menú.
 */
public class AplicacioUB1 
{
    private static  enum OpcionsMenuPrincipal{AFEGIR,ELIMINAR,MOSTRAR,SORTIR};
    private static  String[] desMenuPrincipal = {"Afegeix un fitxer multimedia","Elimina un fitxer multimedia","Mostrar el contingut de la carpeta","Sortir"};
   
    /**
     * Constructor per defecte, buit ja que no hi ha atributs a inicialitzar.
     */
    public AplicacioUB1()
    {

    }  
   
    /**
     * Mètode principal de gestió del programa. Crea un objecte de tipus menu i
     * gesriona l'entrada de l'usuari.
     * @param sc Objecte de tipus Scanner per el qual l'usuari entrarà dades.
     */
    public void gestioAplicacioUB(Scanner sc)
    {
        Menu <OpcionsMenuPrincipal> menuPrincipal = new Menu<>("Menu Principal", OpcionsMenuPrincipal.values());
        menuPrincipal.setDescripcions(desMenuPrincipal);

        CarpetaFitxers carpeta = new CarpetaFitxers();

        OpcionsMenuPrincipal opcio;
       
        do
        {
            menuPrincipal.mostrarMenu();
            opcio = menuPrincipal.getOpcio(sc);

            switch(opcio)
            {
                case AFEGIR:
                {
                    String cami;
                    String descripcio;
                    FitxerMultimedia fitxer;

                    cami = demanarDades(sc, "Introdueixi el camí del fitxer: ");
                    fitxer = new FitxerMultimedia (cami);
                    
                    descripcio = demanarDades(sc, "Introdueixi la descripció del fitxer: ");
                    fitxer.setDescripcio(descripcio);
                    
                    carpeta.addFitxer(fitxer);
                                       
                    break;
                }

                case ELIMINAR:
                {                    
                    System.out.println(carpeta);
                    int index = Integer.parseInt(demanarDades(sc, "Introdueix els index del fitxer que vols eliminar"));
                    carpeta.removeFitxer(carpeta.getAt(index-1));
                    break;
                }

                case MOSTRAR:
                {
                    System.out.println(carpeta);
                    break;
                }

                case SORTIR:
                {
                    System.out.println("Adeu");
                    break;
                }
            }
        }while(opcio != OpcionsMenuPrincipal.SORTIR);
    }
   
    /**
     * Funció per recollir l'entrada de l'usuari
     * @param sc objecte de tipus scanner per l'input de dades
     * @param missatge missatge a mostrar abans de demanar entrada
     * @return l'entrada de l'usuari
     */
    public static String demanarDades(Scanner sc, String missatge)
    {
        String temp;
        System.out.println(missatge);
        temp = sc.nextLine();
        return temp;
    }
}
