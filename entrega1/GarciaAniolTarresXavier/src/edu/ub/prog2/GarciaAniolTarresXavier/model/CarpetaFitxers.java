package edu.ub.prog2.GarciaAniolTarresXavier.model;
import java.util.ArrayList;
import java.io.File;
import java.util.Iterator;

/**
 * La classe CarpetaFitxers és la classe encarregada d'emmagatzemar els
 * FitxersMultimèdia. Conté un ArrayList de tipus File on es guardaran els
 * fitxers i tots els mètodes per gestionar-los.
 */
public class CarpetaFitxers
{
    private ArrayList <File> carpeta;
    private final int MAX_SIZE = 100;
    
    /**
     * Costructor de la classe CarpetaFitxers
     */
    public CarpetaFitxers()
    {
        carpeta = new ArrayList <>();
    }
    
    /**
     * 
     * @return nombre d'elements dins l'ArrayList
     */
    public int getSize()
    {
        return this.carpeta.size();
    }
    
    /**
     * Mètode per afegir un fitxer a l'arrayList
     * @param fitxer Fitxer a afegir
     */
    public void addFitxer(File fitxer)
    {
        if (!isFull())
        {
            carpeta.add(fitxer);
        }
        
        else
        {
            System.out.println("Carpeta plena");
        }
    }
    
    /**
     * Mètode per eliminar la primera instància d'un fitxer
     * @param fitxer Fitxer a eliminar
     */
    public void removeFitxer(File fitxer)
    {
        /*
        boolean trobat = false; //Booleà per comprovar si s'ha eliminat el fitxer
        Iterator <File> carpetaIterator = carpeta.iterator();
        while (carpetaIterator.hasNext()) //Recorrem tota la carpeta
        {
            File element = carpetaIterator.next();
            if (element.equals(fitxer)) //Sempre que coincideix eliminem
            {
                trobat = true;
                carpetaIterator.remove();
                System.out.println("Fitxer eliminat");
            }
        }
        
        if(!trobat) //Si no s'ha trobat el fitxer
        {
            System.out.println("No s'ha trobat el fitxer");
        }
        */
        
        //D'aquest altre mode elimina sol la primera instancia de fitxer
        if(carpeta.remove(fitxer))
        {
            System.out.println("Fitxer eliminat");
        }
        
        else
        {
            System.out.println("No s'ha trobat el fitxer");
        }              
    }
    
    /**
     * Mètode per obtenir el fitxer d'una posició concreta
     * @param position Posició a l'arrayList
     * @return Objecte File a la posició position
     */
    public File getAt(int position)
    {
        if (position>=0 && position < getSize())
        {
            return carpeta.get(position);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Mètode per eliminat tot el contingut de l'arrayList
     */
    public void clear()
    {
        carpeta.clear();
    }
    
    /**
     * Mètode per comprivar si l'arrayList és plena o no
     * @return <code>true</code> si el nombre d'elements de l'arrayList és
     *          superior o igual al màxim i <code>false</code> si n'hi ha menys
     */
    public boolean isFull()
    {
        return this.carpeta.size() >= MAX_SIZE;
    }
    
    /**
     * Sobreescriptura de toString
     * @return String amb tota la informació dels fitxers que conté l'arrayList
     */
    public String toString()
    {
        String resum = "Carpeta Fitxers:\n==============\n\n";
        for (int i = 0; i < carpeta.size(); i++)
        {
            resum += "[" + (i+1) + "] " + carpeta.get(i).toString() +"\n";
        }
        return resum;
    }    
}
