## Problemes irreparables que vec
- Sortir no hauria de tirar excepcio si no esta la finestra oberta
- S'imprimeix dos cops les aplicacioException (s'haura de treure el print, pero quedara un catch sense res a dins molt lleig)

## Changelog
- AplicacioUB3:
  - Canviats els system.err dels input mismatch, potser s'haurien de treure els print de les aplicacioException perque imprimeix dos cops
  - Tanca el reproductor al sortir, tot i que si no esta obert surt una excepció, no ho sé solucionar.
  - Afegit que es mostri quan activem o desactivem la repro ciclica i continuada


- FitxerReproduible:
  - Eliminacio del atribut nom i getNom() perque ja estaven a la classe FitxerMultimedia i aixo provocava que no s'imprimis el nom perque no l'assignavem.


- Controlador:
  - Modificats metodes toggle per a que retornin el bool
