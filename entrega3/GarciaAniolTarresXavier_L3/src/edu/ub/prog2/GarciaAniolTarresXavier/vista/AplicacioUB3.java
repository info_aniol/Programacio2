package edu.ub.prog2.GarciaAniolTarresXavier.vista;
import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Controlador;
import edu.ub.prog2.utils.AplicacioException;
import edu.ub.prog2.utils.Menu;
import java.util.List;
import java.util.Scanner;
import java.util.InputMismatchException;
/**
 * Classe principal del reproductor multimèdia. S'encarrega de gestionar el 
 * menu, l'entrada de l'usuari i la lógica del programa.
 */
public class AplicacioUB3 
{
    //Menu Principal
    private enum OpcionsMenuPrincipal{BIBLIOTECA,ALBUM,CONTROL,GUARDAR,RECUPERAR,SORTIR}

    private static String[] desMenuPrincipal =  {"Gestió Biblioteca", "Gestió Àlbums", "Control Reproducció/Visió",
                                                "Guardar Dades","Recuperar Dades", "Sortir"};
    
    //Menu Gestio de la Biblioteca
    private enum OpcionsMenuBiblioteca{AFEGIR,MOSTRAR,ELIMINAR,ANTERIOR}

    private static String[] desMenuBiblioteca = {"Afegir fitxer multimèdia a la biblioteca", "Mostrar Biblioteca",
                                            "Eliminar fitxer multimèdia", "Menú anterior"};

    //Menu Opcions afegir fitxers a biblioteca
    private enum OpcionsMenuAfegir{VIDEO,AUDIO,ANTERIOR}

    private static String[] desMenuAfegir = {"Afegir vídeo", "Afegir àudio", "Menú anterior"};
    
    //Menu Gestio dels Albums
    private enum OpcionsMenuAlbum{AFEGIR,MOSTRAR,ELIMINAR,GESTIONAR,ANTERIOR}

    private static String[] desMenuAlbum = {"Crear un àlbum", "Mostrar biblioteca d'albums",
                                            "Eliminar àlbum", "Gestionar àlbum", "Menú anterior"};
   
    //Menu Gestionar Album
    private enum OpcionsMenuGestioAlbum{AFEGIR,MOSTRAR,ELIMINAR,ANTERIOR}

    private static String[] desMenuGestioAlbum = {"Afegir fitxer multimèdia a l'àlbum", "Mostrar contingut de l'àlbum",
                                            "Eliminar fitxer multimèdia del àlbum", "Menú anterior"};
    
    //Menu Control Reproducció
    private enum OpcionsMenuControl{REPRODUIR_FITXER,REPRODUIR_BIBLIOTECA,REPRODUIR_ALBUM,TOGGLE_CONTINUADA,TOGGLE_ALEATORIA,GESTIO,ANTERIOR}

    private static String[] desMenuControl =  {"Reprodueix un sol fitxer", "Reproduiex tota la biblioteca", "Reprodueix un àlbum",
                                                "Activa/Desactiva la reproducció continuada","Activa/Desactiva la reproducció aleatoria",
                                                "Gestiona la reproducció en curs","Menú anterior"};
    
    //Menu Gestio Reproducció en curs
    private enum OpcionsMenuReproduccio{REEMPREN,PAUSA,ATURA,SALTA,ANTERIOR}

    private static String[] desMenuReproduccio = {"Continua la reproducció", "Pausa la reproducció",
                                            "Atura la reproducció", "Passa al següent fitxer", "Menú anterior"};
    
    private Menu <OpcionsMenuPrincipal> menuPrincipal;
    private Menu <OpcionsMenuBiblioteca> menuBiblioteca;
    private Menu <OpcionsMenuAfegir> menuAfegir;   
    private Menu <OpcionsMenuAlbum> menuAlbum;
    private Menu <OpcionsMenuGestioAlbum> menuGestioAlbum;
    private Menu <OpcionsMenuControl> menuControl;
    private Menu <OpcionsMenuReproduccio> menuReproduccio;
    
    
    private OpcionsMenuPrincipal opcioPrincipal;    
    private OpcionsMenuBiblioteca opcioBiblioteca;
    private OpcionsMenuAfegir opcioAfegir;   
    private OpcionsMenuAlbum opcioAlbum;
    private OpcionsMenuGestioAlbum opcioGestioAlbum;
    private OpcionsMenuControl opcioControl;
    private OpcionsMenuReproduccio opcioReproduccio;
    
    private Controlador controlador;

   
    /**
     * Constructor per defecte, buit ja que no hi ha atributs a inicialitzar.
     */
    public AplicacioUB3()
    {
        //Creació Menus
        menuPrincipal = new Menu("Menú Principal", OpcionsMenuPrincipal.values());
        menuBiblioteca = new Menu ("Menú Gestio Biblioteca", OpcionsMenuBiblioteca.values());
        menuAfegir = new Menu ("Menú Afegir Fitxer", OpcionsMenuAfegir.values());
        menuAlbum = new Menu("Menú Gestio Albums", OpcionsMenuAlbum.values());
        menuGestioAlbum = new Menu("Menú Gestio un Album", OpcionsMenuGestioAlbum.values());
        menuControl = new Menu("Menú Gestio Control Reproduccio", OpcionsMenuControl.values());
        menuReproduccio = new Menu("Menú Control Reproduccio Actual", OpcionsMenuReproduccio.values());
        
        menuPrincipal.setDescripcions(desMenuPrincipal);
        menuBiblioteca.setDescripcions(desMenuBiblioteca);
        menuAfegir.setDescripcions(desMenuAfegir);
        menuAlbum.setDescripcions(desMenuAlbum);
        menuGestioAlbum.setDescripcions(desMenuGestioAlbum);
        menuControl.setDescripcions(desMenuControl);
        menuReproduccio.setDescripcions(desMenuReproduccio);
        
        //Creacio controlador
        controlador = new Controlador();
    }  
    
    /**
     * Mètode principal de gestió del programa. Crea els menus i gestiona l'entrada
     * de l'usuari.
     * @param sc Scanner
     */
    public void gestioAplicacioUB(Scanner sc)
    {
        controlador.obrirFinestraReproductor();
        do
        {
            menuPrincipal.mostrarMenu();
            opcioPrincipal = menuPrincipal.getOpcio(sc);
            menuPrincipal(sc);
        }while(opcioPrincipal != OpcionsMenuPrincipal.SORTIR);
    }
    
    
    private void menuPrincipal(Scanner sc)
    {
        switch(opcioPrincipal)
        {
            case BIBLIOTECA:
            {
                do
                {
                    menuBiblioteca.mostrarMenu();
                    opcioBiblioteca = menuBiblioteca.getOpcio(sc);
                    menuBiblioteca(sc);                               
                }while(opcioBiblioteca != OpcionsMenuBiblioteca.ANTERIOR);
                break;
            }
            
            case ALBUM:
            {
                do
                {
                    menuAlbum.mostrarMenu();
                    opcioAlbum = menuAlbum.getOpcio(sc);
                    menuAlbum(sc);
                }while(opcioAlbum != OpcionsMenuAlbum.ANTERIOR);
                break;
            }
            
            case CONTROL:
            {
                do
                {
                    menuControl.mostrarMenu();
                    opcioControl = menuControl.getOpcio(sc);
                    menuControl(sc);
                }while(opcioControl != OpcionsMenuControl.ANTERIOR);
                break;
            }     
            
            case GUARDAR:
            {
                String path = demanarDades(sc, "Introdueix el nom del fitxer on guardar les dades");
                try
                {
                    controlador.guardarDadesDisc(path);
                    System.out.println("Dades guardades");
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case RECUPERAR:
            {
                try
                {
                    String path = demanarDades(sc, "Introdueix el nom del fitxer on hi ha les dades a recuperar:");
                    controlador.carregarDadesDisc(path);
                    System.out.println("Dades recuperades");
                }
                catch(AplicacioException e)
                {
                    System.err.print(e.getMessage());
                }
                break;
            }

            case SORTIR:
            {
                controlador.sortir();
                System.out.println("Copia de seguretat feta a \"autoSave.dat\"");
                System.out.println("Adeu");
                break;                    
            }                
        }
    }
    
    private void menuBiblioteca(Scanner sc)
    {
        switch(opcioBiblioteca)
        {
            case AFEGIR:
            {
                do
                {
                    menuAfegir.mostrarMenu();
                    opcioAfegir = menuAfegir.getOpcio(sc);
                    menuAfegir(sc);
                    
                }while(opcioAfegir != OpcionsMenuAfegir.ANTERIOR);
                break;
            }
            case MOSTRAR:
            {
                mostrarCarpeta("Biblioteca Fitxers", controlador.mostrarBiblioteca());
                break;
            }  
            case ELIMINAR:
            {
                System.out.println(controlador.toString());
                int id = demanarDadesInt(sc,"Introduiexi l'índex del fitxer que vol eliminar: ") -1;
                try
                {
                    controlador.esborrarFitxer(id);
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }  
            case ANTERIOR:
            {
                System.out.println("Tornant al menú principal");
                break;                            
            }
        }       
    }
    
    private void menuAfegir(Scanner sc)
    {
        switch(opcioAfegir)
        {
            case VIDEO:
            {                                            
                String cami = demanarDades(sc, "Introdueix el camí del video: ");
                String descripcio = demanarDades(sc, "Introdueix la descripcio del video: ");
                String codec = demanarDades(sc, "Introduiexi el codec del video: ");
                try
                {
                    float durada = demanarDadesFloat(sc,"Introduiexi la durada del video: ");
                    int amplada = demanarDadesInt(sc,"Introdueixi la resolució: [amplada alçada] ");
                    int alcada = demanarDadesInt(sc,"");
                    float fps = demanarDadesFloat(sc,"Introdueixi la framerate: ");
                    try
                    {
                        controlador.afegirVideo(cami, descripcio, codec, durada, alcada, amplada, fps);
                    }
                    catch(AplicacioException e)
                    {
                        System.err.println(e.getMessage());
                    }
                }
                
                catch(InputMismatchException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            case AUDIO:
            {
                String cami = demanarDades(sc, "Introdueix el camí de l'àudio: ");
                String descripcio = demanarDades(sc, "Introdueix la descripcio de l'àudio: ");
                String camiImatge = demanarDades(sc, "Introdueixi el camí de la caràtula: ");
                String codec = demanarDades(sc, "Introduiexi el codec de l'àudio: ");

                try
                {
                    float durada = demanarDadesFloat(sc,"Introduiexi la durada de l'àudio: ");
                    int kbps = demanarDadesInt(sc,"Introdueixi el bitrate (kbps):");

                    try
                    {
                        controlador.afegirAudio(cami, descripcio, camiImatge, codec, durada, kbps);
                    }
                    catch(AplicacioException e)
                    {
                        System.err.println(e.getMessage());
                        
                    }
                }
                catch(InputMismatchException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }  
            case ANTERIOR:
            {
                System.out.println("Tornant al menú gestió");
                break;                            
            }                          
        }       
    }
    
    private void menuAlbum(Scanner sc)
    {
        switch(opcioAlbum)
        {
            case AFEGIR:
            {
                try
                {
                    String titol = demanarDades(sc,"Introdueix el titol del nou album");
                    controlador.afegirAlbum(titol);
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case MOSTRAR:
            {
                mostrarCarpeta("Albums",controlador.mostrarLlistatAlbums());
                break;
            }
            
            case ELIMINAR:
            {
                mostrarCarpeta("Albums", controlador.mostrarLlistatAlbums());
                try
                {
                    String titol = demanarDades(sc,"Introdueix el titol de l'àlbum a eliminar:");
                    controlador.esborrarAlbum(titol);
                }
                catch (AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case GESTIONAR:
            {
                mostrarCarpeta("Albums",controlador.mostrarLlistatAlbums());
                String titol = demanarDades(sc,"Introdueix el titol de l'album que vols gestionar: ");
                if (!controlador.existeixAlbum(titol))
                {
                    System.err.println("No hi ha cap album amb aquest titol");
                }
                else
                {
                    do
                    {
                        menuGestioAlbum.mostrarMenu();
                        opcioGestioAlbum = menuGestioAlbum.getOpcio(sc);
                        menuGestioAlbum(titol,sc);                               
                    }while(opcioGestioAlbum != OpcionsMenuGestioAlbum.ANTERIOR);
                }
                break;
            }
            
            case ANTERIOR:
            {
                System.out.println("Tornant al menú principal");
                break;
            }
        }
    }

    private void menuGestioAlbum(String titol, Scanner sc)
    {
        switch(opcioGestioAlbum)
        {
            case AFEGIR:
            {
                mostrarCarpeta("BibliotecaFitxers", controlador.mostrarBiblioteca());
                try
                {
                    int i = demanarDadesInt(sc,"Introdueix l'index del fitxer que vols afegir") - 1;
                    controlador.afegirFitxer(titol, i);
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                catch(InputMismatchException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case MOSTRAR:
            {
                mostrarCarpeta("Album "+titol, controlador.mostrarAlbum(titol));
                break;
            }
            
            case ELIMINAR:
            {
                try
                {
                    mostrarCarpeta("Album " + titol, controlador.mostrarAlbum(titol));
                    int pos = demanarDadesInt(sc, "Introdueix l'index del fitxer que vols treure de l'album: ") - 1;
                    controlador.esborrarFitxer(titol, pos);
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                catch(InputMismatchException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case ANTERIOR:
            {
                System.out.println("Tornant al menú gestió d'albums");
                break;
            }
        }
    }
    
    
    private void menuControl(Scanner sc)
    {
        switch(opcioControl)
        {
            case REPRODUIR_FITXER:
            {
                try
                {
                    mostrarCarpeta("Biblioteca", controlador.mostrarBiblioteca());
                    int pos = demanarDadesInt(sc, "Introdueix l'índex del fitxer que voleu reproduir: ");
                    controlador.reproduirFitxer(pos-1);
                }
                catch (AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                
                catch(InputMismatchException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case REPRODUIR_BIBLIOTECA:
            {
                controlador.reproduirCarpeta();
                break;
            }
            
            case REPRODUIR_ALBUM:
            {
                mostrarCarpeta("Àlbums", controlador.mostrarLlistatAlbums());
                String title = demanarDades(sc, "Introdueix el nom de l'àlbum que voleu reproduir: ");
                if(!controlador.existeixAlbum(title))
                {
                    System.err.print("No s'ha trobat l'àlbum.");
                }
                else
                {
                    controlador.reproduirCarpeta(title);
                }               
                break;
            }
            
            case TOGGLE_CONTINUADA:
            {
                if(controlador.toggleCiclica())
                {
                    System.out.println("Reproducció cíclica activada");
                }
                else
                {
                    System.out.println("Reproducció cíclica desactivada");
                }
                break;
            }
            
            case TOGGLE_ALEATORIA:
            {
                if(controlador.toggleAleatoria())
                {
                    System.out.println("Reproducció aleatoria activada");
                }
                else
                {
                    System.out.println("Reproducció aleatoria desactivada");
                }
                break;
            }
            
            case GESTIO:
            {
                do
                {
                    menuReproduccio.mostrarMenu();
                    opcioReproduccio = menuReproduccio.getOpcio(sc);
                    menuReproduccio(sc);
                }while(opcioReproduccio != OpcionsMenuReproduccio.ANTERIOR);
                break;
            }
            
            case ANTERIOR:
            {
                System.out.println("Tornant al menú principal");
                break;
            }
        }
    }
    
    private void menuReproduccio(Scanner sc)
    {
        switch(opcioReproduccio)
        {
            case REEMPREN:
            {

                controlador.reemprenReproduccio();
               
                break;
            }
            
            case PAUSA:
            {
                controlador.pausaReproduccio();          
                break;
            }
            
            case ATURA:
            {
                try
                {
                controlador.aturaReproduccio();
                }
                catch(AplicacioException e)
                {
                    System.err.println(e.getMessage());
                }
                break;
            }
            
            case SALTA:
            {
                controlador.saltaReproduccio();
                break;
            }
            
            case ANTERIOR:
            {
                System.out.println("Tornant al menú control");                
                break;
            }
        }
    }
    /**
     * Funció per recollir l'entrada de l'usuari
     * @param sc objecte de tipus scanner per l'input de dades
     * @param missatge missatge a mostrar abans de demanar entrada
     * @return l'entrada de l'usuari
     */
    public static String demanarDades(Scanner sc, String missatge)
    {
        String temp;
        System.out.println(missatge);
        temp = sc.nextLine();
        return temp;
    }
    
    /**
     * Funció per recollir un enter de l'usuari
     * @param sc objecte de tipus scanner per l'input de dades
     * @param missatge missatge a mostrar abans de demanar entrada
     * @return l'entrada de l'usuari
     */
    public static int demanarDadesInt(Scanner sc, String missatge)
    {
        int temp;
        System.out.println(missatge);
        try
        {
            temp = sc.nextInt();
            return temp;

        }
        catch(InputMismatchException e)
        {
            throw new InputMismatchException("Mismatch: El tipus d'entrada és diferent a l'esperat.");
            //sc.nextLine();
        }
        finally
        {
            sc.nextLine();
        }
    }
    
    /**
     * Funció per recollir un float de l'usuari
     * @param sc objecte de tipus scanner per l'input de dades
     * @param missatge missatge a mostrar abans de demanar entrada
     * @return l'entrada de l'usuari
     */
    public static float demanarDadesFloat(Scanner sc, String missatge)
    {
        float temp;
        System.out.println(missatge);
        try
        {
            temp = sc.nextFloat();
            return temp;

        }
        catch(InputMismatchException e)
        {
            throw new InputMismatchException("Mismatch: El tipus d'entrada és diferent a l'esperat.");
            //sc.nextLine();
        }
        finally
        {
            sc.nextLine();
        }
    }
    
    /**
     * Funció per mostrar una llista.
     * @param missatge missatge a mostrar abans d'imprimir la llista, normalment el titol del que s'imprimeix
     * @param carpeta  llista a imprimir
     */
    public static void mostrarCarpeta(String missatge, List<String>carpeta)
    {
        System.out.println(missatge + ":\n==============");
        for (int i = 1; i <= carpeta.size(); i++)
        {
            System.out.println("[" + i + "] " + carpeta.get(i-1));
        }
    }
}
