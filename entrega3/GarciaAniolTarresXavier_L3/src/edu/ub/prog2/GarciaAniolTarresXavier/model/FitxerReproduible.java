package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;
import edu.ub.prog2.utils.AplicacioException;

public abstract class FitxerReproduible extends FitxerMultimedia
{
    protected String codec;
    protected float durada;
    protected transient Reproductor r;
    
    /**
     * Constructor de FitxerReproduible
     * @param cami  Path del fitxer
     * @param codec Codecutilitzat pel fitxer
     * @param durada Durada del contingut multimèdia
     * @param r Reproductor
     */
    protected FitxerReproduible(String cami, String codec, float durada, Reproductor r)
    {
        //No demanem nom perquè ja l'agafem del path a FitxerMultimedia
        super(cami);
        this.codec = codec;
        this.durada = durada;
        this.r = r;
    }
    
    
    /**
     * Getter del codec de l'arxiu multimèdia
     * @return String amb el còdec
     */
    public String getCodec()
    {
        return this.codec;
    }

    /**
     * Getter de la durada de l'arxiu multimèdia
     * @return Float amb la durada
     */
    public float getDurada()
    {
        return this.durada;
    }
    
    /**
     * Setter del reproductor
     * @param r 
     */
    public void setReproductor(Reproductor r)
    {
        this.r = r;
    }
    
    /**
     * Sobeescriputra del mètode toString
     * @return String amb la informació comuna dels FitxerReproduible
     */
    @Override
    public String toString()
    {
        return (super.toString() + " Codec: " + this.getCodec() + "\n Durada: " + this.getDurada() + "\n");
    }
    
    public abstract void reproduir() throws AplicacioException; //Hem de fer que tiri aplicacio exception, perquè el play també la tira
    
}
