package edu.ub.prog2.GarciaAniolTarresXavier.controlador;
import edu.ub.prog2.GarciaAniolTarresXavier.model.*;
import edu.ub.prog2.utils.AplicacioException;
import edu.ub.prog2.utils.InControlador;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Controlador implements InControlador
{
    private Dades dades;
    private EscoltadorReproduccio escoltador;
    private Reproductor reproductor;
    private boolean reproduccioCiclica = false;
    private boolean reproduccioAleatoria = false;

    
    /**
     * Constructor de Controlador
     */
    public Controlador()
    {
        dades = new Dades();
        escoltador = new EscoltadorReproduccio();
        reproductor = new Reproductor(escoltador);
    }
    
    /**
     * Mètode per afegir un vídeo a la biblioteca
     * @param cami Path del arxiu
     * @param descripcio Descripció del vídeo
     * @param codec Codec de vídeo utilitzat
     * @param durada Durada del vídeo
     * @param alcada Alçada del vídeo (en píxels)
     * @param amplada Amplada del vídeo (en píxels)
     * @param fps Framerate del vídeo en frames per segon (FPS)
     * @throws AplicacioException 
     */
    @Override
    public void afegirVideo(String cami, String descripcio, String codec, float durada, int alcada, int amplada, float fps) throws AplicacioException
    {
        try
        {
            dades.addVideo(cami, descripcio, codec, durada, alcada, amplada, fps, this.reproductor);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    
    /**
     * 
     * @param cami Path de l'àudio
     * @param descripcio Descripció de l'àudio
     * @param camiImatge
     * @param codec  El còdec de l'àudio
     * @param durada Dudrada de l'àudio
     * @param kbps  Bitrate (qualitat) de l'àudio en kilobits per second (kbps)
     * @throws AplicacioException 
     */
    @Override
    public void afegirAudio(String cami, String descripcio, String camiImatge, String codec, float durada, int kbps) throws AplicacioException
    {
        try
        {
            dades.addAudio(cami, descripcio, camiImatge, codec, durada, kbps, this.reproductor);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Mètode per eliminar un fitxer donada la seva id
     * @param id Posició que ocupa el fitxer a biblioteca
     * @throws AplicacioException 
     */
    @Override
    public void esborrarFitxer(int id) throws AplicacioException
    {
        try
        {
            dades.removeFitxer(id);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    //Albums
    
    /**
     * Crea un nou àlbum
     * @param titol Titol del nou àlbum
     * @throws AplicacioException 
     */
    @Override
    public void afegirAlbum(String titol) throws AplicacioException
    {
        try
        {
            dades.addAlbum(titol);
        }
        catch (AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Afegeix un fitxer de la biblioteca a un àlbum
     * @param titol Àlbum on afegir el fitxer
     * @param i index del fitxer en la biblioteca
     * @throws AplicacioException 
     */
    @Override
    public void afegirFitxer(String titol, int i) throws AplicacioException
    {
        try
        {
            dades.addFitxer(titol, i);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }

    /**
     * Esborra un àlbum
     * @param titol Titol del àlbum que esborrar
     * @throws AplicacioException 
     */
    @Override
    public void esborrarAlbum(String titol) throws AplicacioException
    {
        try
        {
            dades.removeAlbum(titol);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }

    /**
     * Esborra un fitxer d'un àlbum
     * @param titol Àlbum d'on esborrar
     * @param i Index del fitxer en l'àlbum
     * @throws AplicacioException 
     */
    @Override
    public void esborrarFitxer(String titol, int i) throws AplicacioException
    {
        try
        {
            dades.removeFitxer(titol, i);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }

    /**
     * Comprova si un album existeix o no
     * @param titol Titol del àlbum
     * @return boolean
     */
    @Override
    public boolean existeixAlbum(String titol)
    {
        if (dades.findAlbum(titol) >= 0)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Mètode per obtenir el llistat d'àlbums
     * @return Llista dels titols dels àlbums
     */
    @Override
    public List<String> mostrarLlistatAlbums()
    {
        return dades.mostrarLlistatAlbums();
    }
    
    /**
     * Mètode que retorna una arraylist amb el contingut de l'album
     * @param string String amb el títol de l'album
     * @return ArrayList de String amb el títols
     */
    @Override
    public List<String> mostrarAlbum(String string)
    {
        return dades.mostrarAlbum(string);
    }

    //Reproductor
    
    /**
     * Reproduir un únic fitxer de la biblioteca
     * @param i Index del fitxer a la biblioteca
     * @throws AplicacioException 
     */
    @Override
    public void reproduirFitxer(int i) throws AplicacioException
    {
        ArrayList <File> carpeta = new ArrayList<>();
        if ( i < 0 || i >= dades.getBiblioteca().getSize())
        {
            throw new AplicacioException ("Index fora de rang");
        }
        carpeta.add(this.dades.getBiblioteca().getAt(i));
        CarpetaFitxers reproduir = new CarpetaFitxers(carpeta);
        this.reproduir(reproduir);
    }
    
    /**
     * Obra la finestra del reproductor
     */
    @Override
    public void obrirFinestraReproductor()
    {        
        this.reproductor.open();
    }
    
    /**
     * Tanca la finestra del reproductor
     * @throws AplicacioException 
     */
    @Override
    public void tancarFinestraReproductor() throws AplicacioException
    {
        try
        {
            this.reproductor.close();
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Reprodueix la biblioteca sencera
     */
    @Override
    public void reproduirCarpeta()
    {
        this.reproduir(this.dades.getBiblioteca());
    }
    
    /**
     * Reprodueix un àlbum
     * @param string Titol del àlbum a reproduir
     */
    @Override
    public void reproduirCarpeta(String string)
    {
        AlbumFitxersMultimedia album = this.dades.getAlbum(string);
        this.reproduir(album);
    }
    
    /**
     * Continua la reproducció pausada
     */
    @Override
    public void reemprenReproduccio()
    {
        try
        {
            this.reproductor.resume();
        }
        catch(ClassCastException e)
        {
            //Això passa quan no s'ha inicialitzat cap reproduccio
        }
    }
    
    /**
     * Pausa la reproducció en curs
     */
    @Override
    public void pausaReproduccio()
    {
        try
        {
            this.reproductor.pause();
        }
        catch(ClassCastException e)
        {
            //Això passa quan no s'ha inicialitzat cap reproduccio
        }
    }
    
    /**
     * Atura la reproducció en curs
     * @throws AplicacioException 
     */
    @Override
    public void aturaReproduccio() throws AplicacioException
    {
        try
        {
            this.reproductor.stop();
        }
        catch(ClassCastException e) //Si no s'esta reproduint res
        {   
            throw new AplicacioException("No s'està reproduint res");
        }
    }
    
    /**
     * Passa al següent fitxer de la reproducció en curs
     */
    @Override
    public void saltaReproduccio()    
    {
        this.escoltador.next();
    }   
    
    /**
     * Canvia l'estat de reproduccioAleatoria al contrari del actual
     * @return Estat final 
     */
    public boolean toggleAleatoria()
    {
        this.reproduccioAleatoria = !this.reproduccioAleatoria;
        return this.reproduccioAleatoria;
    }
    
    /**
     * Canvia l'estat de reproduccioCiclica al contrari del actual
     * @return Estat final 
     */
    public boolean toggleCiclica()
    {
        this.reproduccioCiclica = !this.reproduccioCiclica;
        return this.reproduccioCiclica;
    }
    
    //Guardar i recuperar dades
    
    /**
     * Metode que llegeix recupera les dades d'una altra biblioteca.
     * @param path //
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    @Override
    public void carregarDadesDisc(String path) throws AplicacioException
    {
        File file = new File(path);
        try
        {
            dades = Dades.recover(file);
            dades.recoverReproductor(reproductor);
        }

        catch(AplicacioException e)
        {
            throw e;
        }
    }
        
    /**
     * Metode que crea un fitxer on guardar les dades actuals. Es crea un fitxer
     * amb extensió .data que despres es podra recuperar.
     * @param fileName nom del fitxer
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    @Override
    public void guardarDadesDisc(String fileName) throws AplicacioException
    {
        if (fileName.isEmpty())
        {
            fileName = "MyData";
        }
        File file = new File(fileName + ".dat");
        try
        {
            dades.save(file);
        } 
        catch (AplicacioException e)
        {
            throw new AplicacioException("AplicacioException: " + e.getMessage());
        }
    }
    
    /**
     * Mètode per recopilar tota la informació de cadascun dels fitxers
     * @return List de Strings amb els toString de cada ftxer
     */
    @Override
    public List<String> mostrarBiblioteca()
    {
        return dades.mostrarBiblioteca();        
    }
    
    /**
     * Sobreescriptura del mètode toString
     * @return String amb tota la informació del controlador (dades)
     */
    @Override
    public String toString()
    {
        return dades.toString();
    }
    
    /**
     * Inicia la reproducció de una CarpetaFitxers, amb l'estat de la reproduccio Ciclica i Aleatoria del moment
     * @param carpeta Carpeta a reproduir
     */
    public void reproduir(CarpetaFitxers carpeta)
    {
        this.escoltador.iniciarReproduccio(carpeta, this.reproduccioCiclica, this.reproduccioAleatoria);
    }
    
    /**
     * Fa una còpia de les dades i tanca la finestra del reproductor.
     * Aquesta funció es crida al acabar el programa
     */
    public void sortir()
    {
        try
        {
            this.guardarDadesDisc("autoSave");
            this.tancarFinestraReproductor();
        }
        catch(AplicacioException e)
        {
            
        }
    }
}
