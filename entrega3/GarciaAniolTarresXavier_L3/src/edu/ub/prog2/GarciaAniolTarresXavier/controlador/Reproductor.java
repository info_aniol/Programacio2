package edu.ub.prog2.GarciaAniolTarresXavier.controlador;

import edu.ub.prog2.GarciaAniolTarresXavier.model.*;
import edu.ub.prog2.utils.*;
import java.io.*;

public class Reproductor extends ReproductorBasic
{

    /**
     * Constructor de la classe Reproductor
     * @param controlador EscoltadorReproduccioBasic
     */
    public Reproductor(EscoltadorReproduccioBasic controlador)
    {
        super(controlador);
    }
    
    /**
     * Reprodueix un fitxer
     * @param fr Fitxer
     * @throws AplicacioException 
     */
    public void reprodueix(FitxerReproduible fr) throws AplicacioException
    {
        try
        {
            super.play(fr);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Reprodueix un fitxer Audio amb caràtula
     * @param audio Audio a reproduir
     * @param fitxerImatge Caràtula a mostrar
     * @throws AplicacioException 
     */
    public void reprodueix(Audio audio, File fitxerImatge) throws AplicacioException
    {
        try
        {
            super.play(audio, fitxerImatge);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
}
