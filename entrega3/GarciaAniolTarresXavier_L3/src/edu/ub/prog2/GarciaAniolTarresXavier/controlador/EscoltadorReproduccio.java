package edu.ub.prog2.GarciaAniolTarresXavier.controlador;
import edu.ub.prog2.GarciaAniolTarresXavier.model.*;
import edu.ub.prog2.utils.*;
import java.util.*;


public class EscoltadorReproduccio extends EscoltadorReproduccioBasic
{
    private CarpetaFitxers llistaReproduint = null;
    private boolean [] llistaCtr; //Llista per comprovar reporducció aleatòria
    private boolean reproduccioCiclica, reproduccioAleatoria;
    private boolean [] errors; //Llista per comprovar els fitxers no reproduibles
    private int cont;
    
    /**
     * Inicialitza els parametres del que es vol reproduir i comença a reproduir
     * @param llistaReproduint Carpeta que s'ha de reproduir
     * @param reproduccioCiclica 
     * @param reproduccioAleatoria 
     */
    public void iniciarReproduccio(CarpetaFitxers llistaReproduint, boolean reproduccioCiclica, boolean reproduccioAleatoria)
    {
        this.llistaReproduint = llistaReproduint;
        this.reproduccioCiclica = reproduccioCiclica;
        this.reproduccioAleatoria = reproduccioAleatoria;
        this.llistaCtr = new boolean[llistaReproduint.getSize()];
        Arrays.fill(llistaCtr, false);
        cont = 0;
        
        this.next();
    }

    /**
     * S'executa automaticament al acabar de reproduir un fitxer.
     * Crida a reproduir al següent
     */
    @Override
    protected void onEndFile() 
    {
        this.next();
    }

    /**
     * Reprodueix el següent fitxer.
     */
    @Override
    protected void next() 
    {
        if(this.hasNext()) //Si té següent
        {
            if(this.reproduccioAleatoria)
            {
                //Si la reproducció es aleatoria triem una posicio aleatoriament
                int pos = (int)Math.round(Math.random()*(this.llistaCtr.length -1));
                //Mentres aquesta ja hagi estat vista sumem 1
                while(this.llistaCtr[pos])
                {
                    pos = (pos+1)%this.llistaReproduint.getSize();
                }
                
                try
                { 
                    //Quan troba una no vista la veiem i canviem la llista de control d'aquella posicio a true
                    this.llistaCtr[pos] = true;
                    this.cont++;
                    ((FitxerReproduible)this.llistaReproduint.getAt(pos)).reproduir();
                }
                catch(AplicacioException e)
                {
                    this.next();
                }    
            }
            else
            {                    
                try
                { 
                    ((FitxerReproduible)this.llistaReproduint.getAt(cont)).reproduir();
                    this.cont++;

                }
                catch(AplicacioException e)
                {
                    this.next();
                } 
            }
        }
        //Si no te següent i la reproduccio es ciclica
        else if(reproduccioCiclica && llistaReproduint != null) //El null es per si ens quedem sense fitxers per culpa d'errors
        {
            this.reset();
            this.next();
        } 
    }

    /**
     * Mira si estem al final de la llista.
     * @return 
     */
    @Override
    protected boolean hasNext() 
    {
        if (llistaReproduint != null)
        {
            return (this.cont < this.llistaReproduint.getSize());
        }
        return false;
    }
           
    
    /**
     * Mètode per fer un reset dels fitxers Reproduits
     */
    private void reset()
    {
        Arrays.fill(this.llistaCtr, false);
        this.cont = 0;
    }
}
