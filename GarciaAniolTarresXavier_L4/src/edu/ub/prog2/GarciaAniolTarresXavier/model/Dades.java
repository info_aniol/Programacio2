package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.GarciaAniolTarresXavier.controlador.Reproductor;
import java.io.File;
import java.io.Serializable;
import edu.ub.prog2.utils.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dades implements Serializable
{
    private CarpetaFitxers biblioteca;
    private ArrayList<AlbumFitxersMultimedia> albums;
    /**
     * Constructor per defecte de Dades.
     */
    public Dades()
    {
        biblioteca = new BibliotecaFitxersMultimedia();
        albums = new ArrayList<>();
    }
    
    
    //Biblioteca
    /**
     * Metode per afegir un video a la biblioteca.
     * @param cami
     * @param descripcio
     * @param codec
     * @param durada
     * @param alcada
     * @param amplada
     * @param fps
     * @param r
     * @throws edu.ub.prog2.utils.AplicacioException
     */
    public void addVideo(String cami, String descripcio, String codec, float durada, int alcada, int amplada, float fps, Reproductor r) throws AplicacioException 
    {
        Video video = new Video(cami, codec, durada, alcada, amplada, fps, r);
        video.setDescripcio(descripcio);
        try
        {
            biblioteca.addFitxer(video);
        }
        catch(AplicacioException e) //Si es llença una excpeció la tornem a tirar amunt.
        {
            throw e;
        }
    }
    
    /**
     * Mètode per afegir un àudio a la biblioteca
     * @param cami Cami de l'audio
     * @param descripcio Descripcio del fitxer
     * @param camiImatge
     * @param codec
     * @param durada
     * @param kbps
     * @param r
     * @throws AplicacioException Si el fitxer no existeix
     */
    public void addAudio(String cami, String descripcio, String camiImatge, String codec, float durada, int kbps, Reproductor r) throws AplicacioException
    {
        File fitxerImatge = new File(camiImatge);
        if(!fitxerImatge.exists())
        {
            fitxerImatge = new File("black.png");
        }
        Audio audio = new Audio(cami, fitxerImatge, codec, durada, kbps, r);
        audio.setDescripcio(descripcio);
        try
        {
            biblioteca.addFitxer(audio);
        }
        catch(AplicacioException e)
        {
            throw e;
        }

    }
    
    
    /**
     * Mètode per eliminar un fitxer donada la seva id
     * @param id Posició que ocupa el fitxer a biblioteca
     * @throws AplicacioException Ens passem de rang
     */
    public void removeFitxer(int id) throws AplicacioException
    {
        try
        {
            File fitxer = biblioteca.getAt(id);
            for (int i = 0; i < this.albums.size(); i++)
            {
                AlbumFitxersMultimedia album = this.albums.get(i);
                while(album.inFolder(fitxer))
                {
                    album.removeFitxer(fitxer);
                }
            }
            biblioteca.removeFitxer(id);

        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Mètode per recopilar tota la informació de cadascun dels fitxers
     * @return List de Strings amb els toString de cada ftxer
     */
    public List<String> mostrarBiblioteca()
    {
        return biblioteca.mostrar();
    }
    
    
    /**
     * Mètode per crear un nou àlbum
     * @param title String amb el titol del nou àlbum
     * @throws AplicacioException L'àlbum ja existeix
     */
    public void addAlbum(String title) throws AplicacioException
    {
        if (findAlbum(title) != -1)
        {
            throw new AplicacioException ("Ja existeix un àlbum amb aquest titol");
        }
        AlbumFitxersMultimedia album = new AlbumFitxersMultimedia(title);
        this.albums.add(album);
    }
    
    /**
     * Elimina un àlbum 
     * @param title Títol del àlbum a eliminar
     * @throws AplicacioException L'àlbum no existeix
     */
    public void removeAlbum(String title) throws AplicacioException
    {
        int pos = findAlbum(title);
        if (pos == -1)
        {
            throw new AplicacioException ("No existeix un album amb aquest nom");
        }
        this.albums.remove(pos);
    }
    
    /**
     * Mètode per afegir un fitxer a un àlbum
     * @param title String amb el títol de l'Àlbum on es vol afegir el fitxer
     * @param i Enter amb la ID del fitxer que es vol afegir
     * @throws AplicacioException Si ens pasem de posicio o l'àlbum no existeix
     */
    public void addFitxer(String title, int i) throws AplicacioException
    {
        if(i<0 || i>biblioteca.getSize())
        {
            throw new AplicacioException("Index del fitxer fora de rang");
        }
        
        int j = findAlbum(title);
        //No cal comprobrar que existeix l'album perque ja es comprova abans d'entrar al menu gestionar

        try
        {
            albums.get(j).addFitxer(biblioteca.getAt(i));
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Esborra un fitxer d'un àlbum
     * @param title Titol del àlbum
     * @param pos Posició del fitxer en l'àlbum
     * @throws AplicacioException Si ens pasem de posicio
     */
    public void removeFitxer(String title, int pos) throws AplicacioException
    {
        int j = findAlbum(title);
        //No cal comprobrar que existeix l'album perque ja es comprova abans d'entrar al menu gestionar
        
        try
        {
            albums.get(j).removeFitxer(pos);
        }
        catch(AplicacioException e)
        {
            throw e;
        }
    }
    
    /**
     * Mètode per trobar la ID d'un Àlbum
     * @param title String amb el títol de l'Àlbum
     * @return Enter amb la ID de l'Àlbum
     */
    public int findAlbum(String title)
    {
        for (int i = 0; i<albums.size(); i++)
        {
            if (title.equals(albums.get(i).getTitle()))
            {
                return i;
            }
        }
        return -1; //En cas que no el troba
    }
    
    /**
     * Mètode que retorna una llista de string amb els fitxers d'un àlbum
     * @param titol Titol del àlbum a mostrar
     * @return Llista dels toString dels fitxers
     */
    public List<String> mostrarAlbum(String titol)
    {
        int id = findAlbum(titol);
        //No cal comprobrar que existeix l'album perque ja es comprova abans d'entrar al menu gestionar
        return this.albums.get(id).mostrar();
        
    }
    
    /**
     * Mètode per recopilar els titols dels diferents albums
     * @return List de Strings amb els noms dels albums
     */
    public List<String> mostrarLlistatAlbums()
    {
        List<String> titols = new ArrayList<>();
        for(int i = 0; i < this.albums.size(); i++)
        {
            titols.add(this.albums.get(i).getTitle()); 
        }
        return titols;
    }
    
    
    
    //Control de les dades
    
    /**
     * Metode per guardar la dades
     * @param f Fitxer on guardar les dades
     * @throws AplicacioException Qualsevol error que es pugui produir
     */
    public void save(File f) throws AplicacioException
    {
        
        FileOutputStream fileStream = null;
        ObjectOutputStream os = null;
        try
        {
            //Intentem obrir el fitxer i carregar-hi l'objecte Dades actual
            fileStream = new FileOutputStream(f);
            os = new ObjectOutputStream(fileStream);
            os.writeObject(this);
        }
        catch(FileNotFoundException e) //Si no es troba el fitxer
        {
            throw new AplicacioException("Fitxer no trobat");            
        }
        catch(IOException e) //Si no es pot obrir o escriure-hi
        {
            throw new AplicacioException("Problema d'escriptura");
        }
        finally //Hagi tirar excepció o no
        {
            try
            {
                if(fileStream != null) //Si el fitxer no és null (si s'ha obert)
                {
                    fileStream.close(); //Tanquem el fitxer
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar el fitxer");
            }
            
            try
            {
                if(fileStream != null)
                {
                    os.close(); //Tanquem l'ObjectStream
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar l'Output Stream");
            }
        } 
    }
    
    /**
     * Metode per recuperar les dades d'un fitxer
     * @param f fitxer d'on recuperar les dades
     * @return Objecte Dades amb les nostres dades
     * @throws AplicacioException Qualsevol error que es pugui donar
     */
    public static Dades recover(File f) throws AplicacioException
    {
        Dades d;
        InputStream fileStream = null;
        ObjectInputStream oi = null ;
        try
        {
            //Intentem obrir el fitxer
            fileStream = new FileInputStream(f);
            oi = new ObjectInputStream(fileStream);
            d = (Dades)oi.readObject(); //Llegim l'objecte i fem cast implícit a dades
            return d; //Retornem un objecte de tipus Dades
        }
        catch(FileNotFoundException e)
        {
            throw new AplicacioException("Fitxer no trobat");            
        }
        catch(IOException e)
        {
            throw new AplicacioException("No s'ha pogut recuperar");
        } 
        catch (ClassNotFoundException e) 
        {
            throw new AplicacioException("Class not found");
        }
        finally
        {
            try
            {
                if(fileStream != null)
                {
                    fileStream.close(); //Intentem tancar el fitxer
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar el fitxer");
            }
            try
            {
                if(fileStream != null)
                {
                    oi.close(); //Intentem tancar l'ObjectStream
                }
            }
            catch(IOException e)
            {
                throw new AplicacioException("No s'ha pogut tancar l'Input Stream");
            }
            catch(NullPointerException e)
            {
                throw new AplicacioException("Error en el fitxer");             
            }
        }
    }
    
    /**
     * Mètode per tornar a tenir un Reproductor en els fitxers al recuperar les dades, ja que aquest no es guarda.
     * @param r Reproductor
     */
    public void recoverReproductor (Reproductor r)
    {
        for(int i = 0; i < biblioteca.getSize(); i++)
        {
            File fitxer = biblioteca.getAt(i);
            if (fitxer instanceof FitxerReproduible)
            {
                ((FitxerReproduible) fitxer).setReproductor(r);
            }
        }
    }
    
    /**
     * Getter de la biblioteca
     * @return BibliotecaFitxers
     */
    public CarpetaFitxers getBiblioteca()
    {
        return biblioteca;
    }
    
    /**
     * Getter de la llista d'albums.
     * @return Array d'àlbums
     */
    public ArrayList<AlbumFitxersMultimedia> getAlbums()
    {
        return albums;
    }
    
    /**
     * Getter d'un àlbum
     * @param title Titol de l'àlbum
     * @return Àlbum
     */
    public AlbumFitxersMultimedia getAlbum(String title)
    {
        return this.albums.get(findAlbum(title));
    }
    
    /**
     * Sobreescriptra del mètode toString
     * @return Una única String amb tota la informació dels fitxers.
     */
    @Override
    public String toString()
    {
        String s;
        s = biblioteca.toString();
        for (int i = 0; i < albums.size(); i++)
        {
            s += albums.get(i).toString();
        }
        return biblioteca.toString();
    }
}

