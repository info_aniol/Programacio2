package edu.ub.prog2.GarciaAniolTarresXavier.model;

import edu.ub.prog2.utils.AplicacioException;
import java.io.File;
import java.util.*;

public class AlbumFitxersMultimedia extends CarpetaFitxers
{
    private String titol;
    
    /**
     * Costructor bàsic d'Album. Posa la mida màxima per defecte
     * @param titol String amb el títol de l'album a crear
     */
    public AlbumFitxersMultimedia(String titol)
    {
        super(10);
        this.titol = titol;
    }
    
    /**
     * Constructor parametritzat amb títol i mida
     * @param titol String amb el títol de l'album
     * @param max  Enter amb el màxim d'entrades que permet l'àlbum
     */
    public AlbumFitxersMultimedia(String titol, int max)
    {
        super(max);
        this.titol = titol;
    }
    
   
    //Setters
    /**
     * Setter del titol de l'album
     * @param title nou titol
     */
    public void setTitle(String title)
    {
        this.titol = title;
    }
    
    //Getters
    /**
     * Getter del titol de l'album
     * @return 
     */
    public String getTitle()
    {
        return this.titol;
    }
    
    /**
     * Sobreescriptura de toString
     * @return String amb tota la informació dels fitxers que conté l'arrayList
     */
    @Override
    public String toString()
    {
        String resum = "Album "+titol+":\n==============\n\n";
        for (int i = 0; i < carpeta.size(); i++)
        {
            resum += "[" + (i+1) + "] " + carpeta.get(i).toString() +"\n";
        }
        return resum;
    }    
}
